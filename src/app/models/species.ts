class Species{
    name: string;
    classification: string;
    designation: string;
    people: string[];
    films: string[];
}
