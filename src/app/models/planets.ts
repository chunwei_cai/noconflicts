class Planet{
    name: string;
    rotation_period: string;
    orbital_period: string;
    diameter: string;
    films: string[];
}