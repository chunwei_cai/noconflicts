import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  rootUrl = "https://swapi.co/api";

  constructor(private http: HttpClient) { }


  getCategoryData(category:string, num: number): Observable<People|Planet|Species|Starship>{
    console.log("url", `${this.rootUrl}/${category}/${num}`);
    return this.http.get<People|Planet|Species|Starship>(`${this.rootUrl}/${category}/${num}`).pipe(retry(3), catchError(this.handleError) ); 
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message)
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`)
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.')
  }
}
