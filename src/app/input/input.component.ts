import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DataService } from '../data.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {
  categoryArray = ['people', 'planets', 'vehicles', 'species', 'starships']
  selectedCategory : string
  selectedNumber
  results;
  error;

  constructor(private dataService: DataService) { 
    
  }

  onSubmit(){
    this.selectedNumber = (<HTMLInputElement>document.getElementById("number")).value;
   
    this.dataService.getCategoryData(this.selectedCategory, this.selectedNumber).subscribe((results) => {
      console.log("result: " + results);
      this.results = results;
      this.error = null;
      //this.startEvent();
    }, (error)=>{
      console.log("error: " + error)
      this.error = error;
      this.results = null;
    })
  }

  ngOnInit() {
  }

}
