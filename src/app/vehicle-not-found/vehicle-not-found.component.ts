import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vehicle-not-found',
  templateUrl: './vehicle-not-found.component.html',
  styleUrls: ['./vehicle-not-found.component.css']
})
export class VehicleNotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
